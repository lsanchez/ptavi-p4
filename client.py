#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import sys
import socket

# Constantes. Dirección IP del servidor y contenido a enviar
try:
    server = sys.argv[1]
    port = int(sys.argv[2])
    register = sys.argv[3]
    direc = sys.argv[4]
    expires = sys.argv[5]
    if register != 'register':
        sys.exit("Debes introducir 'register'")
except IndexError:
    sys.exit("Usage: client.py ip puerto register sip_address expires_value")

SIP = (register.upper() + " sip:"+direc+" SIP/2.0\r\nExpires: " +
       expires + "\r\n\r\n")
# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((server, port))
    print("Enviando:", SIP)
    my_socket.send(bytes(SIP, 'utf-8'))
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
