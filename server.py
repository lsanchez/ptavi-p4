#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import sys
import socketserver
import json
import time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    dic = {}

    def json2register(self):
        try:
            with open('registered.json', 'r') as fichjson:
                self.dic = json.load(fichjson)
        except (FileNotFoundError):
            pass

    def register2json(self):

        with open('registered.json', 'w') as fichjson:
            json.dump(self.dic, fichjson, indent=4)

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        self.json2register()
        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")

        line = self.rfile.read()
        line = line.decode('utf-8').split()
        if line[0] == 'REGISTER':
            direccion = line[1].split(':')
            usuario = direccion[1]
            ip = self.client_address[0]
            expires = line[4]
            Time = time.time() + int(expires)
            TimeExp = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(Time))
            now = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))
            self.dic[usuario] = {'address: ': ip, 'expires': TimeExp}
            if line[4] == '0' or TimeExp <= now:
                del self.dic[usuario]
                print(self.dic)
            else:
                print(self.dic)

        self.register2json()


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    ip, puerto = sys.argv[1:]
    serv = socketserver.UDPServer((ip, int(puerto)), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
